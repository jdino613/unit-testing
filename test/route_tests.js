const chai = require("chai"); // assertion lib
const { expect } = chai; // style
const chaiHttp = require("chai-http"); // request handler

// SERVER URL
const serverUrl = "http://localhost:3000";

// TEST SUITE
describe("GET ALL RATES", () => {
    it("should accept http requests", () => {
        chai
        .request(serverUrl)
        .get("/rates")
        .end((err,res) => expect(res).to.not.equal(undefined)
    })


    it("should have a status of 200", (done) => {
        chai.request(serverUrl)
        .get("/rates")
        .end(err,res) => {
            expect(res).to.have.status(200)
            done()
        })
    })

    it("should have an object w/ a property of rates", () => {
        chai.request(serverUrl)
        .get("/rates")
        .end((err,res) => {
            expect(res.body).to.have.property('rates')
            done()
        })
    })

    it("should have an object w/ a property of rates w/ the length of 5", () => {
        chai.request(serverUrl)
        .get("/rates")
        .end((err,res) => {
            expect(Object.keys(res.body.rates).length).equals(5)
            done()
        })
    })
})

// CREATE A TEST SUITE TO GET ALL CURRENCIES
    describe("GET ALL CURRENCIES", () => {
        it("should accept http requests", () => {
            chai
            .request(serverUrl)
            .get("/currencies")
            .end((err,res) => expect(res).to.not.equal(undefined)
        })

        it("should have a status of 200", (done) => {
            chai.request(serverUrl)
            .get("/currencies")
            .end((err,res) => {
                expect(res).to.have.status(200)
            })
        })

        it("should have an object w/ a property of currencies", () => {
            chai.request(serverUrl)
            .get("/currencies")
            .end((err,res) => {
                expect(res.body).to.have.property('currencies')
                done()
            })
        })
    
        it("should have an object w/ a property of currencies w/ the length of 5", () => {
            chai.request(serverUrl)
            .get("/currencies")
            .end((err,res) => {
                expect(Object.keys(res.body.currencies)).lengthOf(5)
                done()
            })
        })

        it("should have an object w/ a property of currencies in an array", () => {
            chai.request(serverUrl)
            .get("/currencies")
            .end((err,res) => {
                expect(res.body.currencies).to.be.an('array')
                done()
            })
        })  

        describe("GET ALL CURRENCIES", () => {
            describe("THAT IS USD", () => {
                it("should have status 200", () => {
                    chai
                    .request(serverUrl)
                    .get("/currencies/usd")
                    .end((err,res) => expect(res).to.have.status(200))
                })
            })

            it("should have property usd", (done) => {
                chai.request(serverUrl)
                .get("/currencies/usd")
                .end((err,res, req) => {
                    expect(res.body.usd).to.have.property('')
                    done()
                })
            }) 
            
            it("should have a nested property name", (done) => {
                chai.request(serverUrl)
                .get("/currencies/usd")
                .end((err, req) => {
                    expect(res.body.usd).to.have.property('name')
                    done()
                })
            })  
            
})
