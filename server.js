//PACKAGES AND VARIABLES
const express = require("express");
const app = express();
const PORT = 3000;

//MIDDLEWARE
app.use(express.json());

// RESOURCE
const currencies = require("./routes/currencies.js");
app.use("/currencies", currencies);

//ROUTE
app.use("/", (req, res) => {
  res.send({ data: {} });
});

//INITIALIZE THE SERVER
app.listen(PORT, () => {
  console.log(`App is running on port ${PORT}.`);
});
