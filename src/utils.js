const exchangeRates = {
	'usd': {
		'name': 'United States Dollar',
		'ex': {
			'peso': 50.73,
			'won': 1187.24,
			'yen': 108.63,
			'yuan': 7.03
		}
	},
	'yen': {
		'name': 'Japanese Yen',
		'ex': {
			'peso': 0.47,
			'won': 0.0092,
			'yen': 10.93,
			'yuan': 0.065
	},
	'peso': {
		'name': 'Philippine Peso',
		'ex': {
			'peso': 0.020,
			'won': 23.39,
			'yen': 2.14,
			'yuan': 0.14
	},
	'yuan': {
		'name': 'Chinese Yuan',
		'ex': {
			'peso': 7.21,
			'won': 0.14,
			'yen': 168.85,
			'yuan': 15.45
	},
	'won': {
		'name': 'United States Dollar',
		'ex': {
			'peso': 0.043,
			'won': 0.00084,
			'yen': 0.092,
			'yuan': 0.0059
	}
};


const exchange = (baseCurrency, targetCurrency, amount) => {
	return exchangeRates[baseCurrency].ex[targetCurrency] * amount;
}

module.exports = {exchangeRates, exchange}